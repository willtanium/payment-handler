import functools
import requests

__author__ = 'william'


class POST:
    def __init__(self, end_point):
        self.__end_point = end_point
        self.__callback = None
        self.__headers = {'Content-Type': 'text/xml',
                          'Content-transfer-encoding': 'text'}
        self.__body = None
        self.__response = None

    def __call__(self, fn):
        @functools.wraps(fn)
        def decorated(*args, **kwargs):
            result = fn(*args, **kwargs)
            try:
                if result['body'] is not None:
                    self.__body = result['body']
                if result['callback'] is not None:
                    self.__callback = result['callback']

                self.__post_handler()
            except TypeError, e:
                print str(e)
        return decorated

    def __post_handler(self):
        if self.__body is not None:
            print("END POINT {0} \n \nDATA{1}\n".format(self.__end_point, self.__body))
            self.__response = requests.post(url=self.__end_point, headers=self.__headers,
                                            data=self.__body, verify=False)

            if self.__callback is not None:
                self.__callback(response=self.__response.content, code=self.__response.status_code)


class GET:
    def __init__(self, end_point):
        self.__end_point = end_point
        self.__callback = None
        self.__headers = {'Content-Type': 'text/xml',
                          'Content-transfer-encoding': 'text'}
        self.__body = None
        self.__response = None

    def __call__(self, fn):
        @functools.wraps(fn)
        def decorated(*args, **kwargs):
            result = fn(*args, **kwargs)
            for key, value in result.iteritems():
                if key is 'body':
                    self.__body = value
                elif key is 'callback':
                    self.__callback = value
                self.__get_handler()

        return decorated

    def __get_handler(self):
        if self.__body is not None:
            self.__response = requests.post(url=self.__end_point, headers=self.__headers,
                                            data=self.__body, verify=False)
            if self.__callback is not None:
                self.__callback(response=self.__response.content, code=self.__response.status_code)

