from wsgiref import simple_server
import falcon
from resource.request_handlers import TestPayment, NotificationCallback, YoPaymentFailureHandler

__author__ = 'william'
TEST_NUMBER = '0787075772'
TEST_AMOUNT = '1000'
TEST_MESSAGE = 'Test deduction message'
HOST = "localhost"
PORT = 9000

# def test_payment():
#     payment_details = PaymentDetails(amount=int(TEST_AMOUNT), account=TEST_NUMBER, reference_id=str(uuid4())[:4],
#                                      message=TEST_MESSAGE)
#     yo_payment = YoPayments()
#     yo_payment.make_deposit(payment_details=payment_details)


app = falcon.API()

pay_out = TestPayment()
notice_callback = NotificationCallback()
failure_notice_callback = YoPaymentFailureHandler()
app.add_route('/pay_out', pay_out)
app.add_route('/yo_notice', notice_callback)
app.add_route('/failure_notice', failure_notice_callback)

if __name__ == '__main__':

    httpd = simple_server.make_server(HOST, PORT, app)
    httpd.serve_forever()
