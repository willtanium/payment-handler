from datetime import datetime
from xml.parsers import expat
from network_handlers.network_wrappers import POST
from transaction_handlers.payment_settings import YO_PAYMENTS, NOTIFICATION_URL, FAILURE_NOTIFICATION_URL, \
    YO_PAYMENT_URL, MPESA_PAYMENTS, MPESA_NOTIFICATION_URL, MPESA_URL


__author__ = 'william'


class Payments(object):
    """
        This Class has abstract methods that are to be used by all payment modes that are available
        MTN , MPESA ..etc
    """

    def send_query(self, payment_data, callback):
        """
            This creates the request body the fits the format
        :param payment_data:
        :param callback:
        :return:
        """
        self.validate_xml(payment_data)
        return {'body': payment_data, 'callback': callback}

    def current_time(self):
        """
            Method for to get the current server time
        :return:
        """
        return datetime.now().strftime("%Y%m%d%H%M%S")

    def payment_callback(self, response, code):
        """
            The abstract callback handler for all payments transactions
        :param response:
        :param code:
        :return:
        """
        pass

    def validate_xml(self, string):
        try:
            parser = expat.ParserCreate()
            parser.Parse(string)
            print("Well Formatted XML")
        except Exception, e:

            print("Poorly formatted XML {0}".format(e))


class YoPayments(Payments):
    def __init__(self):
        self.__body = '<?xml version="1.0" encoding="UTF-8"?>' \
                      '<AutoCreate>' \
                      '<Request>' \
                      '<APIUsername>' + YO_PAYMENTS['username'] + '</APIUsername>' \
                                                                  '<APIPassword>' + YO_PAYMENTS[
                          'password'] + '</APIPassword>' \
                                        '{0}' \
                                        '</Request>' \
                                        '</AutoCreate>'

    def get_body(self):
        return self.__body

    @POST(YO_PAYMENT_URL)
    def make_deposit(self, payment_details):
        data = "<Method>acdepositfunds</Method>" \
               "<NonBlocking></NonBlocking>" \
               "<Amount>{0}</Amount>" \
               "<Account>{1}</Account>" \
               "<Narrative>{2}</Narrative>" \
               "<ExternalReference>{6}</ExternalReference>" \
               "<InstantNotificationUrl>{3}</InstantNotificationUrl>" \
               "<FailureNotificationUrl>{4}</FailureNotificationUrl>" \
               "<ProviderReferenceText>{5}</ProviderReferenceText>". \
            format(payment_details.amount, self.__number_formatter(payment_details.account),
                   payment_details.message, NOTIFICATION_URL,
                   FAILURE_NOTIFICATION_URL, payment_details.message, payment_details.reference_id)

        final_data = self.get_body().format(data)

        return self.send_query(payment_data=final_data, callback=self.payment_callback)

    @POST(YO_PAYMENT_URL)
    def transfer_money(self, transfer_details):
        data = "<Method>acinternaltransfer</Method>" \
               "<CurrencyCode>{0}</CurrencyCode>" \
               "<Amount>{1}</Amount>" \
               "<BeneficiaryAccount>{2}</BeneficiaryAccount>" \
               "<BeneficiaryEmail>{3}</BeneficiaryEmail>" \
               "<Narrative>{4}</Narrative>". \
            format(transfer_details.currency_code,
                   transfer_details.amount,
                   self.__number_formatter(transfer_details.receiver),
                   transfer_details.email,
                   transfer_details.message)

        data = self.get_body().format(data)
        return self.send_query(payment_data=data, callback=self.payment_callback)

    @POST(YO_PAYMENT_URL)
    def check_balance(self):
        data = "<Method>acacctbalance</Method>"
        data = self.get_body().format(data)
        return self.send_query(payment_data=data, callback=self.payment_callback)

    @POST(YO_PAYMENT_URL)
    def get_mini_statement(self):
        data = "<Method>acgetministatement</Method>"
        data = self.get_body().format(data)
        return self.send_query(payment_data=data, callback=self.payment_callback)

    @POST(YO_PAYMENT_URL)
    def payment_callback(self, response, code):
        print("Server response {0} \n code {1} \n".format(response, code))

    def __number_formatter(self, number):
        number = number.replace('+', '')
        if number[:3].__contains__('256') is False:
            print("Number for modding {0}".format(number))
            number = bytearray(number, 'utf8')
            del number[0]
            number = '256' + str(number)
            print("Modded Number {0}".format(number))
            return number
        else:
            return number


class MpesaPayments(Payments):
    def __init__(self):
        self.__body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"xmlns:tns="tns:ns">' \
                      '<soapenv:Header>' \
                      '<tns:CheckOutHeader>' \
                      '<MERCHANT_ID>' + MPESA_PAYMENTS['merchant_id'] + '</MERCHANT_ID>' \
                                                                        '<PASSWORD>' + MPESA_PAYMENTS[
                          'password'] + '</PASSWORD>' \
                                        '<TIMESTAMP>' + self.current_time() + '</TIMESTAMP>' \
                                                                              '</tns:CheckOutHeader>' \
                                                                              '</soapenv:Header>' \
                                                                              '<soapenv:Body>' \
                                                                              '{0}' \
                                                                              '</soapenv:Body>' \
                                                                              '</soapenv:Envelope>'

    def get_body(self):
        return self.__body

    @POST(MPESA_URL)
    def make_deposit(self, payment_details):
        data = '<tns:processCheckOutRequest>' \
               '<MERCHANT_TRANSACTION_ID>{0}</MERCHANT_TRANSACTION_ID>' \
               '<REFERENCE_ID>{1}</REFERENCE_ID>' \
               '<AMOUNT>{2}</AMOUNT>' \
               '<MSISDN>{3}</MSISDN>' \
               '<ENC_PARAMS></ENC_PARAMS>' \
               '<CALL_BACK_URL>{4}</CALL_BACK_URL>' \
               '<CALL_BACK_METHOD>xml</CALL_BACK_METHOD>' \
               '<TIMESTAMP>{5}</TIMESTAMP>' \
               '</tns:processCheckOutRequest>'.format(payment_details.transaction_id, payment_details.reference_id,
                                                      payment_details.amount, payment_details.account,
                                                      MPESA_NOTIFICATION_URL, self.current_time())

        data = self.get_body().format(data)
        return self.send_query(payment_data=data, callback=self.payment_callback)

    def payment_callback(self, response, code):
        print("Server response {0} \n code {1} \n".format(response, code))
