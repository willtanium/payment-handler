__author__ = 'william'


class PaymentDetails(object):
    def __init__(self, account, amount, message=None, transaction_id=None, reference_id=None):
        self.__account = account
        self.__amount = amount
        self.__message = message
        self.__transaction_id = transaction_id
        self.__reference_id = reference_id

    @property
    def account(self):
        return self.__account

    @property
    def amount(self):
        return self.__amount

    @property
    def message(self):
        return self.__message

    @property
    def transaction_id(self):
        if self.__transaction_id is None:
            return ''
        else:
            return self.__transaction_id

    @property
    def reference_id(self):
        if self.__reference_id is None:
            return ''
        else:
            return self.__reference_id


class TransferDetails(object):
    def __init__(self, amount, receiver, currency_code=None, email=None, message=None):
        self.__currency_code = currency_code
        self.__amount = amount
        self.__receiver = receiver
        self.__email = email
        self.__message = message

    @property
    def amount(self):
        return self.__amount

    @property
    def receiver(self):
        return self.__receiver

    @property
    def currency_code(self):
        if self.__currency_code is None:
            return ''
        else:
            return self.__currency_code

    @property
    def email(self):
        if self.__email is None:
            return ''
        else:
            return self.__email

    @property
    def message(self):
        if self.__message is None:
            return ''
        else:
            return self.__message