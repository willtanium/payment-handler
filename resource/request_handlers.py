import json
from pprint import pprint
from uuid import uuid4
import falcon
from falcon.util.uri import parse_query_string
from transaction_handlers.payments import YoPayments
from transfer_objects.data_objects import PaymentDetails

__author__ = 'william'

TEST_MESSAGE = 'Test deduction message'


class TestPayment(object):
    def __init__(self):
        self.__payment = None

    def on_post(self, req, resp):
        self.__payment = json.loads(req.stream.read())
        payment_details = PaymentDetails(amount=int(self.__payment['amount']),
                                         account=self.__payment['account'],
                                         reference_id=str(uuid4())[:4],
                                         message=TEST_MESSAGE)

        yo_payment = YoPayments()
        yo_payment.make_deposit(payment_details=payment_details)
        resp.body = json.dumps({'status': 'SUCCESS', 'message': 'Payment Executed'})
        resp.content_type = 'application/json'
        resp.status = falcon.HTTP_200


class NotificationCallback(object):
    def __init__(self):
        self.__amount = None
        self.__payment_number = None
        self.__network_ref = None
        self.__external_ref = None
        self.__narrative = None
        self.__signature = None
        self.__datetime = None

    def on_post(self, req, resp):
        print("Amount Test %s" % req.get_param_as_int('amount'))
        self.__amount = req.get_param_as_int('amount')
        self.__payment_number = req.get_param('msisdn')
        self.__network_ref = req.get_param('network_ref')
        self.__external_ref = req.get_param('external_ref')
        self.__signature = req.get_param('signature')
        self.__datetime = req.get_param('date_time')
        print("{0} {1} {2} {3} {4}".format(self.__amount, self.__payment_number, self.__network_ref))
        resp.status = falcon.HTTP_200


class YoPaymentFailureHandler(object):
    def __init__(self):
        self.__datetime = None
        self.__failed_reference = None
        self.__verification = None
        self.database_handler = None
        pass

    def on_post(self, req, resp, params):
        self.__datetime = params['transaction_init_date']
        self.__failed_reference = params['failed_transaction_reference']
        self.__verification = params['verification']

        resp.content_type = 'application/json'
        resp.status = falcon.HTTP_200

